<!DOCTYPE html>
<html class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Restaurant</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- CSS
        ================================================ -->
        <!-- Owl Carousel -->
		<link rel="stylesheet" href="css/owl.carousel.css">
        <!-- bootstrap.min css -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Font-awesome.min css -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="css/animate.min.css">

		<link rel="stylesheet" href="css/main.css">
        <!-- Responsive Stylesheet -->
		<link rel="stylesheet" href="css/responsive.css">
		<!-- Js -->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="js/jquery.nav.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
	</head>
	<body>
	<!--
	header-img start 
	============================== -->
    <section id="hero-area">
      <img class="img-responsive" src="images/header.jpg" alt="">
    </section>
	<!--
    Header start 
	============================== -->
	<nav id="navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <nav class="navbar navbar-default">
                          <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </button>
                                  <a class="navbar-brand" href="#">
                                    <img src="images/logo.png" alt="Logo">
                                  </a>

                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                              <ul class="nav navbar-nav navbar-right" id="top-nav">
                                <li><a href="#hero-area">Home</a></li>
                                <li><a href="#about-us">About us</a></li>
                                
                                <li><a href="#order">Order Food</a></li>
                                
                                <li><a href="#price">Menu</a></li>
                                <li><a href="#contact-us">Contacts</a></li>
                              </ul>
                            </div><!-- /.navbar-collapse -->
                          </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                </div><!-- .col-md-12 close -->
            </div><!-- .row close -->
        </div><!-- .container close -->
	</nav><!-- header close -->
    <!--
    Slider start
    ============================== -->
    <section id="slider">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                        <div class="title">
                            <h3>More <span>Details</span></h3>
                        </div>
                        <div id="owl-example" class="owl-carousel">
                            <div>
                                <img class="img-responsive" src="images/luwombo.jpg" alt="">
                            </div>
                            <div>
                                <img class="img-responsive" src="images/chicken.jpg" alt="">
                            </div>
                            <div>
                                <img class="img-responsive" src="images/quicklunch.jpg" alt="">
                            </div>
                            <div>
                                <img class="img-responsive" src="images/beans.jpg" alt="">
                            </div>
                            <div>
                                <img class="img-responsive" src="images/Double-Meat-Breakfast.jpg" alt="">
                            </div>
                            <div>
                                <img class="img-responsive" src="images/break.jpg" alt="">
                            </div>
                            <div>
                                <img class="img-responsive" src="images/specials.jpg" alt="">
                            </div>
                            <div>
                                <img class="img-responsive" src="images/slider/slider-img-4.jpg" alt="">
                            </div>
                        
                        </div>
                    </div>
                </div><!-- .col-md-12 close -->
            </div><!-- .row close -->
        </div><!-- .container close -->
    </section><!-- slider close -->
    <!--
    about-us start
    ============================== -->
    <section id="about-us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <img class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="400ms" src="images/cooker-img.png" alt="cooker-img">
                        <h1 class="heading wow fadeInUp" data-wow-duration="400ms" data-wow-delay="500ms" ><span>Real Taste Restaurant</span>
                        </h1>
                        <p class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="600ms">We are located in the heart of Kampala next to Ministry of Finance ,Planning and Economic Development. Thi strategic position provides a quiet and ideal environment for Conferences, weddings, Workshops and Parties. </br>Real Taste Restaurant is managed under Ministry of Finance and it serves meals to only registered members of the Minstry.  </br></p>
                    </div>
                </div><!-- .col-md-12 close -->
            </div><!-- .row close -->
        </div><!-- .containe close -->
    </section><!-- #call-to-action close -->
    <!--
    Order Food start
    ============================== -->
    <section id="order">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="popup" id="order" class="ui-content" style="min-width:250px;">
                      <h1 class="heading wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span>ORDER FOOD HERE</span></h1>
                       <div class="block">
                            <form method="post" action="">
                            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay="600ms">
                            <label> Names :</label>
                                <input type="text" name="names" class="form-control" id="names" placeholder="Full Names" required="True">
                            </div>
                           
                             <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay="600ms">
                            <label>Phone Number :</label>
                                <input type="number" name="phonenumber" class="form-control" id="phonenumber" placeholder="Phone Number" required="True">
                            </div>
                            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay="800ms">
                            <label>Email :</label>
                                <input name="email" type="email" class="form-control" placeholder="Write your email address here..." required="True">
                            </div>
                            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay="600ms">
                                <label>Choose any type of Meal :</label>
                                <select id="meals" name="meals">
                                  <option value="chips and liver">Chips and Liver</option>
                                  <option value="rice and beans">Rice and Beans</option>
                                  <option value="chips and pilao">Chips and Pilao</option>
                                  <option value="meat and rice">Meat and Rice</option>
                                   <option value="None">None</option>
                                </select>
                            </div>
                             <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay="600ms">
                                <label>Choose any type of Breakfast :</label>
                                <select id="breakfast" name="breakfast">
                                   <option value="milk tea">Milk Tea</option>
                                  <option value="black tea">Black Tea</option>
                                  <option value="chapati">Chapati</option>
                                  <option value="samosa">Samosa</option>
                                  <option value="None">None</option>
                                </select>
                            </div>
                             <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay="600ms">
                                <label>Choose any type of Drinks :</label>
                                <select id="drinks" name="drinks">
                                  <option value="rwenzori">Rwenzori</option>
                                  <option value="soda">Soda</option>
                                  <option value="juice">Juice</option>
                                  <option value="cocktail">Cock Tail</option>
                                   <option value="None">None</option>
                                </select>
                            </div>
                            <div> <button class="btn btn-lg btn-success btn-block" id="send" name="send">Send Order</button></div>
                        </form>
                       <?php
                       if (isset($_POST['send'])) {
                        include('db.php');
                        $names = $_POST['names'];
                        $phonenumber = $_POST['phonenumber'];
                        $email = $_POST['email'];
                        $meals = $_POST['meals'];
                        $breakfast = $_POST['breakfast'];
                        $drinks = $_POST['drinks'];
                        

                         $result = mysql_query("INSERT INTO `order`(`names`, `phonenumber`, `email`, `meals`, `breakfast`, `drinks`) VALUES ('$names','$phonenumber','$email','$meals','$breakfast','$drinks')");
                        
                       }
                       ?>
            
                    </div>
                  </div>
                </div><!-- .col-md-12 close -->
            </div><!-- .row close -->
        </div><!-- .containe close -->
    </section><!-- #call-to-action close -->
   
    <!--
    price start
    ============================ -->
    <section id="price">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h1 class="heading wow fadeInUp" data-wow-duration="300ms" data-wow-delay="300ms"><span>MENU</span> AND <span>PRICE</span></h1>
                        <p class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="400ms">We serve the best dishes in the country for every tribe and region such Matooke, Kalo, Eshabwe,Luwombo e.t.c</p>
                        <div class="pricing-list">
                            <div class="title">
                                <h3> <span>Our Menu of the Day</span></h3>
                            </div>
                            <ul>
                        <?php 
                        include 'db.php';

                        $list = '';

                        $result=mysql_query("SELECT `id`, `name`, `description`, `price` FROM `menu` WHERE 1");
                                    if ($result==false) {
                                        # code...
                                        die(mysql_error());
                                    }
                    
                       while ($test = mysql_fetch_array($result)) {
                        
                        $list = '
                         <li class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="300ms">
                                    <div class="item">
                                        <div class="item-title">
                                            <h2>'.$test['name'].'</h2>
                                            <div class="border-bottom"></div>
                                            <span>'.$test['price'].'shs</span>
                                        </div>
                                        <p>'.$test['description'].'</p>
                                    </div>
                                </li>
                ';
                echo $list;

            }
                        
            
                mysql_close($conn);
                                
                                ?>
                            </ul>
                           
                        </div>
                    </div>
                </div><!-- .col-md-12 close -->
            </div><!-- .row close -->
        </div><!-- .containe close -->
    </section><!-- #price close -->
    <!--
       CONTACT US  start
    ============================= -->
    <section id="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h1 class="heading wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms"><span>CONTACT US</span></h1>
                        <h3 class="title wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">Sign Up for <span>Email Alerts</span> </h3>
                        <form method="post" action="">
                            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay="600ms">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Write your full name here..." required="True">
                            </div>
                            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay="800ms">
                                <input type="email" class="form-control"
                                name="email" placeholder="Write your email address here..." required="True">
                            </div>
                            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay="1000ms">
                                <textarea class="form-control" rows="3" name="message" placeholder="Write your message here..." required="True"></textarea>
                            </div>
                       
                        <div><button class="btn btn-lg btn-success btn-block" id="send" name="submit">Send Message</button></div>
                         </form>
                         <?php
                       if (isset($_POST['submit'])) {
                        include('db.php');
                        $name = $_POST['name'];
                        $email = $_POST['email'];
                        $message = $_POST['message'];
                        
                        

                         $result = mysql_query("INSERT INTO `contact`(`name`, `email`, `message`) VALUES ('$name','$email','$message')");
                       
                       }
                       ?>
            
                    </div>
                </div><!-- .col-md-12 close -->
            </div><!-- .row close -->
        </div><!-- .container close -->
    </section><!-- #contact-us close -->
    <!--
    footer  start
    ============================= -->
    <section id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="block wow fadeInLeft"  data-wow-delay="200ms">
                        <h3> <span> CONTACT INFO</span></h3>
                        <div class="info">
                            <ul>
                                <li>
                                  <h4><i class="fa fa-phone"></i>Telephone</h4>
                                  <p>+256703523626</p>
                                    
                                </li>
                                <li>
                                  <h4><i class="fa fa-map-marker"></i>Address</h4>
                                  <p>Ministry of Finance Uganda</p>
                                </li>
                                <li>
                                  <h4><i class="fa fa-envelope"></i>E mail</h4>
                                  <p>realtaste@gmail.com</p>
                                  
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- .col-md-4 close -->
                   <!--
    footer-bottom  start
    ============================= -->
    <footer id="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="block">
                        <p>Copyright &copy; 2016 - All Rights Reserved.Design and Developed By Asiimire Hildah</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	</body>
</html>