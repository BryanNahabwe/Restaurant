-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2016 at 07:36 PM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(5) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `message` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `message`) VALUES
(1, 'memem', 'nfhfnfh@ghtyt.omv', 'nhfhyfyhghgngjgjgjjgjgjgjgjg'),
(2, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(3, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(4, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(5, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(6, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(7, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(8, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(9, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(10, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(11, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(12, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(13, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(14, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(15, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(16, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(17, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(18, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(19, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(20, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(21, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(22, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(23, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(24, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(25, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(26, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(27, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(28, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(29, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(30, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(31, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(32, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(33, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(34, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(35, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(36, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(37, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(38, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(39, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(40, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(41, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj'),
(42, 'memem', 'nahabwebryn@outlook.com', 'bgaggaffsfsfsgddhjfjfjgkhkhlj');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `price` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `description`, `price`) VALUES
(4, 'Matooke', 'Matooke is grown every 2 seasons in a year', 30000),
(5, 'Rice', 'Ricee is very niceeeee', 20000),
(6, 'Pizza', 'Pizza is backed using floor', 12000);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(5) NOT NULL,
  `names` varchar(250) NOT NULL,
  `phonenumber` int(10) NOT NULL,
  `email` varchar(250) NOT NULL,
  `meals` varchar(250) NOT NULL,
  `breakfast` varchar(250) NOT NULL,
  `drinks` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `names`, `phonenumber`, `email`, `meals`, `breakfast`, `drinks`) VALUES
(1, 'bryan', 703831504, 'nahabwebryn@hotmail.com', 'rice and beans', 'black tea', 'drinks'),
(2, 'tadz', 702467771, 'tadz@gmsil.com', 'chips and liver', 'None', 'drinks'),
(3, 'hildah', 703523626, 'hildah@yahoo.com', 'chips and liver', 'milk tea', 'drinks');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(5) NOT NULL,
  `names` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phonenumber` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `names`, `username`, `password`, `email`, `phonenumber`, `address`) VALUES
(1, 'nahabwe bryan', 'bryan', 'bryan123', 'nahabwebrian24@gmail.com', '0703831504', 'makindye'),
(2, 'Mike Baron', 'baron', 'baron123', 'baron@gmail.com', '0703745550', 'kampala'),
(3, 'kabz deno', 'deno', 'deno123', 'kabzdeno@gmail.com', '0708474645', 'nakawa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
